package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePage extends BasePage
{
    public ProfilePage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        super(driver, wait);
    }

    @AndroidFindBy(xpath = "//*[@text='Giriş yap']")
    private MobileElement loginBtn;

    @AndroidFindBy(id = "android:id/message")
    private MobileElement welcomeMessage;

    public boolean isDisplayedProfilePage()
    {
        return loginBtn.isDisplayed();
    }

    public LoginPage clickLogin()
    {
        loginBtn.click();
        return new LoginPage(driver, wait);
    }

    public boolean isDisplayedWelcomeMessage()
    {
        return welcomeMessage.isDisplayed();
    }

    public String getTextWelcomeMessage()
    {
        return welcomeMessage.getText();
    }
}
