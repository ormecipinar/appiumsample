package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage
{

    public HomePage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        super(driver, wait);
    }

    @FindBy(id = "com.pozitron.hepsiburada:id/account_icon")
    private MobileElement profileBtn;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout[@content-desc=\"Kategoriler\"]/android.widget.ImageView")
    private MobileElement categories;

    public ProfilePage clickProfileBtn()
    {
        profileBtn.click();
        return new ProfilePage(this.driver, wait);
    }

    public CategoriesPage clickCategoriesBtn()
    {
        categories.click();
        return new CategoriesPage(this.driver, wait);
    }
}
