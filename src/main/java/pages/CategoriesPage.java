package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CategoriesPage extends BasePage
{
    public CategoriesPage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        super(driver, wait);
    }


    @AndroidFindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.pozitron.hepsiburada:id/recyclerViewChildCategory']" +
            "/android.view.ViewGroup//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup")
    private List<MobileElement> categories;

    public ProductCategoriesPage clickFirstCategory()
    {
        categories.get(0).click();
       // waitForProgressBar();
        return new ProductCategoriesPage(driver, wait);
    }
}
