package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductCategoriesPage extends BasePage
{
    public ProductCategoriesPage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        super(driver, wait);
    }

    @AndroidFindBy(accessibility= "samsung_tel")
    private MobileElement samsungProduct;

    public ProductListPage clickProduct(){
        samsungProduct.click();
        return new ProductListPage(driver,wait);
    }
}
