package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasketPage extends BasePage
{
    public BasketPage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        super(driver, wait);
    }

    @AndroidFindBy(xpath = "//android.view.View[@resource-id='onboarding_item_list']/android.widget.ListView/android.view.View")
    private List<MobileElement> basketItem;

    public boolean isDisplayedBasketItem(int index)
    {
        return basketItem.get(index).isDisplayed();
    }

    public int sizeOfBasketItems()
    {
        return basketItem.size();
    }
}
