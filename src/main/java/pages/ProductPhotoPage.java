package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPhotoPage extends BasePage
{
    public ProductPhotoPage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        super(driver, wait);
    }

    @AndroidFindBy(accessibility = "Geri")
    private MobileElement backBtn;

    public boolean isDisplayedBack(){
        return backBtn.isDisplayed();
    }
    public ProductListPage clickBack()
    {
        backBtn.click();
        return new ProductListPage(driver, wait);
    }
}
