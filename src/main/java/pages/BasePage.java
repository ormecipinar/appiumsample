package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

@Slf4j
public class BasePage
{
    public AndroidDriver<MobileElement> driver;
    public WebDriverWait wait;

    public BasePage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    @AndroidFindBy(xpath = "com.pozitron.hepsiburada:id/progressBar")
    private MobileElement progressBar;

    @FindBy(id = "com.pozitron.hepsiburada:id/com_appboy_inappmessage_modal_graphic_bound")
    private MobileElement layer;

    @AndroidFindBy(id = "com.pozitron.hepsiburada:id/com_appboy_inappmessage_modal_close_button")
    private MobileElement closeLayer;

    public void waitForProgressBar()
    {
        wait.until(ExpectedConditions.invisibilityOf(progressBar));
    }

    public <T extends BasePage> T waitForLayer(T page)
    {
        try
        {
            wait.until(ExpectedConditions.visibilityOf(layer));
            if (layer.isDisplayed())
                closeLayer.click();
        }
        catch (Exception e)
        {
            log.info("Layer is not seen!");
        }
        return page;
    }

    public void swipe()
    {
        Dimension size = driver.manage().window().getSize();
        System.out.println(size.height + "height");
        System.out.println(size.width + "width");
        System.out.println(size);
        int startPoint = (int) (size.width * 0.99);
        int endPoint = (int) (size.width * 0.15);
        int ScreenPlace = (int) (size.height * 0.40);
        int y = (int) size.height * 20;

        TouchAction ts = new TouchAction(driver);

        ts.press(PointOption.point(startPoint, ScreenPlace))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
                .moveTo(PointOption.point(endPoint, ScreenPlace)).release().perform();
    }
}
