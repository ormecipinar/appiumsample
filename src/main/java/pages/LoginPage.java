package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage
{
    public LoginPage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        super(driver, wait);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@resource-id='txtUserName']")
    private MobileElement userName;

    @AndroidFindBy(xpath = "//android.widget.EditText[@resource-id='txtPassword']")
    private MobileElement password;

    @AndroidFindBy(xpath = "//android.widget.Button[@resource-id='btnLogin']")
    private MobileElement submitBtn;

    @AndroidFindBy(xpath = "//android.view.View[@text='Girdiğiniz bilgiler ile herhangi bir hesabı eşleştiremedik, kontrol edip tekrar deneyin.']")
    private MobileElement failMessage;

    public <T extends BasePage> T login(T page, String username, String pasw)
    {
        userName.sendKeys(username);
        password.sendKeys(pasw);
        submitBtn.click();
        return page;
    }

    public boolean isDisplayedLoginScreen()
    {
        return userName.isDisplayed();
    }

    public LoginPage waitForLoginPage()
    {
        wait.until(ExpectedConditions.visibilityOf(userName));
        return this;
    }

    public boolean isDisplayedFailMessage() throws InterruptedException
    {
        Thread.sleep(1000L);
        return failMessage.isDisplayed();
    }
}
