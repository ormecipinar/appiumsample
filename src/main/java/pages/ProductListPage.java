package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ProductListPage extends BasePage
{
    public ProductListPage(AndroidDriver<MobileElement> driver, WebDriverWait wait)
    {
        super(driver, wait);
    }

    @AndroidFindBy(accessibility = "ürün fotoğrafı")
    private MobileElement productPhoto;

    @AndroidFindBy(accessibility = "Sepete Ekle")
    private MobileElement addToBasket;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout[@resource-id='com.pozitron.hepsiburada:id/lyt_modal_view_content']")
    private MobileElement addToBasketFrame;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout[@resource-id='com.pozitron.hepsiburada:id/lyt_modal_view_content']/android.view.ViewGroup/android.widget.TextView")
    private List<MobileElement> frameBtns;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout[@content-desc=\"Sepet\"]/android.widget.ImageView")
    private MobileElement basket;

    public ProductPhotoPage clickPhoto()
    {
        productPhoto.click();
        return new ProductPhotoPage(driver, wait);
    }

    public ProductListPage addToBasket()
    {
        addToBasket.click();
        return this;
    }

    public boolean isDisplayedAddToBasketFrame()
    {
        return addToBasketFrame.isDisplayed();
    }

    public ProductListPage add()
    {
        frameBtns.get(0).click();
        return this;
    }

    public BasketPage clickBasket()
    {
        basket.click();
        return new BasketPage(driver, wait);
    }

}
