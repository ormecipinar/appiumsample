package extension;

import annotation.MyDriver;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import utils.ScreenshotUtils;

@Slf4j
public class TestWatcherExtension implements BeforeEachCallback, AfterEachCallback, BeforeAllCallback, AfterAllCallback, TestWatcher
{
    WebDriver driver;

    @Override
    public void testAborted(ExtensionContext extensionContext, Throwable throwable)
    {
        log.info("Test Aborted -> {}{}{}: ", extensionContext.getTestClass(), extensionContext.getDisplayName());
    }

    @Override
    public void testDisabled(ExtensionContext extensionContext, Optional<String> optional)
    {
        log.info("Test Disabled -> {}{}{}: ", extensionContext.getTestClass(), extensionContext.getDisplayName());
    }

    @Override
    public void testFailed(ExtensionContext extensionContext, Throwable throwable)
    {
        log.error("Test Failed -> {}{}{}: ", extensionContext.getTestClass(), extensionContext.getDisplayName());
    }

    @Override
    public void testSuccessful(ExtensionContext extensionContext)
    {
        log.info("Test Successful -> {}{}{}: ", extensionContext.getTestClass(), extensionContext.getDisplayName());
    }

    @Override
    public void afterAll(ExtensionContext extensionContext)
    {
    }

    @Override
    public void afterEach(ExtensionContext extensionContext)
    {
        Class currentClass = null;
        try
        {
            currentClass = extensionContext.getTestClass().orElseThrow(() -> new Exception("Current class not found"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        do
        {
            Field[] fields = Objects.requireNonNull(currentClass).getDeclaredFields();
            Arrays.stream(fields)
                    .forEach(f ->
                            Optional.of(f.isAnnotationPresent(MyDriver.class))
                                    .filter(Boolean::booleanValue)
                                    .ifPresent(d -> injectFieldValues(extensionContext, f)));

            currentClass = currentClass.getSuperclass();
        }
        while (!currentClass.equals(Object.class));

        ScreenshotUtils.takeSnapShotOnFailure(extensionContext, this.driver);

        this.driver.quit();
    }

    @Override
    public void beforeAll(ExtensionContext extensionContext)
    {
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext)
    {
        log.info("Test Started -> {}{}{}: ", extensionContext.getTestClass(), extensionContext.getDisplayName());
    }


    private void injectFieldValues(ExtensionContext extensionContext, Field field)
    {
        try
        {
            field.setAccessible(true);
            this.driver = (WebDriver) field.get(extensionContext.getRequiredTestInstance());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
