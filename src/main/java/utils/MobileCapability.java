package utils;

public enum MobileCapability
{
    DEVICE_NAME("deviceName"),
    UDID("udid"),
    PLATFORM_NAME("platformName"),
    PLATFORM_VERSION("platformVersion"),
    APP("app"),
    SKIP_UNLOCK("skipUnlock"),
    NO_RESET("noReset"),
    ENFORCE_APP_INSTALL("enforceAppInstall");

    private String capability;

    MobileCapability(String capability)
    {
        this.capability = capability;
    }

    public String getCapability(){
        return capability;
    }
}
