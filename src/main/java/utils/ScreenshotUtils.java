package utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;

import java.io.File;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class ScreenshotUtils
{
    public static void takeSnapShotOnFailure(ExtensionContext extensionContext, WebDriver driver)
    {
        if (extensionContext.getExecutionException().isPresent())
        {
            try
            {
                SessionId sessionid = ((RemoteWebDriver) driver).getSessionId();
                extensionContext.getRoot().getStore(ExtensionContext.Namespace.create(ScreenshotUtils.class)).put("sessionId", String.valueOf(sessionid));

                TakesScreenshot scrShot = ((TakesScreenshot) driver);
                File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
                File DestFile = new File(getScreenshotPath() + "/" + getScreenshotFileName(extensionContext, sessionid));
                FileUtils.copyFile(SrcFile, DestFile);

            }
            catch (Exception e)
            {
                log.error("screenshot failed : {}", e.getMessage());
            }
            log.info("--- screenshot taken for test---" + extensionContext.getRequiredTestMethod().getName());
        }
    }

    private static String getScreenshotFileName(ExtensionContext extensionContext, SessionId sessionid)
    {
        String classname = extensionContext.getRequiredTestClass().getName();
        String testName = extensionContext.getRequiredTestMethod().getName();
        String name = classname.concat(".").concat(testName);

        AtomicReference<Method> testMethod = new AtomicReference<>();
        extensionContext.getTestMethod().ifPresent(testMethod::set);
        return (name.concat(".") + String.valueOf(sessionid).concat(".png"));
    }

    private static String getScreenshotPath()
    {
        String defaultValue = "Screenshots/RunningLocalHBTest";
        ifNotExistCreateDIR(defaultValue);
        return defaultValue;
    }

    public static void ifNotExistCreateDIR(String dir)
    {
        try
        {
            Files.createDirectories(Paths.get(dir));
        }
        catch (Exception ex)
        {
            log.info("there is no such file so it has not been deleted : {}", dir);
        }
    }
}
