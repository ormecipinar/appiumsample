package tests;

import annotation.MyDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.FileResourceUtils;
import utils.MobileCapability;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class BaseTest
{
    @MyDriver
    public AndroidDriver<MobileElement> driver;
    public WebDriverWait wait;

    @BeforeEach
    public void before() throws MalformedURLException, URISyntaxException
    {
        FileResourceUtils app = new FileResourceUtils();
        String fileName = "apks/hepsiburada.apk";
        File file = app.getFileFromResource(fileName);

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapability.DEVICE_NAME.getCapability(), "Galaxy_S20_FE");
        capabilities.setCapability(MobileCapability.UDID.getCapability(), "RF8NA15VY8A");//DeviceId from "adb devices" command
        capabilities.setCapability(MobileCapability.PLATFORM_NAME.getCapability(), "Android");
        capabilities.setCapability(MobileCapability.PLATFORM_VERSION.getCapability(), "11.0");
        capabilities.setCapability(MobileCapability.SKIP_UNLOCK.getCapability(), "true");
        capabilities.setCapability("appPackage", "com.pozitron.hepsiburada");
        capabilities.setCapability("appActivity", "com.hepsiburada.ui.startup.SplashActivity");
        capabilities.setCapability(MobileCapability.APP.getCapability(), file.getAbsolutePath());
        //capabilities.setCapability(MobileCapability.ENFORCE_APP_INSTALL.getCapability(), "true");
        capabilities.setCapability(MobileCapability.NO_RESET.getCapability(), "false");
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        wait = new WebDriverWait(driver, 15);
    }
}
