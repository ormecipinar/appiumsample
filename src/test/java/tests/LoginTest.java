package tests;

import extension.TestWatcherExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import pages.HomePage;
import pages.LoginPage;
import pages.ProfilePage;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

import static utils.ConstantUtils.PASSWORD;
import static utils.ConstantUtils.SUCCESS_MESSAGE;
import static utils.ConstantUtils.USERNAME;
import static utils.ConstantUtils.WRONG_PASSWORD;

@ExtendWith(TestWatcherExtension.class)
@Tag("run")
public class LoginTest extends BaseTest
{
    private HomePage homePage;
    private ProfilePage profilePage;
    private LoginPage loginPage;

    @Override
    @BeforeEach
    public void before() throws MalformedURLException, URISyntaxException
    {
        super.before();
        homePage = new HomePage(driver, wait);
        profilePage = homePage.waitForLayer(homePage)
                .clickProfileBtn();
        Assertions.assertTrue(profilePage.isDisplayedProfilePage());

        loginPage = profilePage.clickLogin()
                .waitForLoginPage();
        Assertions.assertTrue(loginPage.isDisplayedLoginScreen());
    }

    @Test
    public void successfulLogin()
    {
        profilePage = loginPage.login(profilePage, USERNAME, PASSWORD);
        ProfilePage finalProfilePage = profilePage;
        Assertions.assertAll(
                () -> Assertions.assertTrue(finalProfilePage.isDisplayedWelcomeMessage()),
                () -> Assertions.assertEquals(SUCCESS_MESSAGE, finalProfilePage.getTextWelcomeMessage())
        );
    }

    @Test
    public void failedLogin() throws InterruptedException
    {
        profilePage = loginPage.login(profilePage, USERNAME, WRONG_PASSWORD);
        Assertions.assertTrue(loginPage.isDisplayedFailMessage());
    }
}
