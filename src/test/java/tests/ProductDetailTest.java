package tests;

import extension.TestWatcherExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import pages.BasketPage;
import pages.CategoriesPage;
import pages.HomePage;
import pages.ProductCategoriesPage;
import pages.ProductListPage;
import pages.ProductPhotoPage;

import java.util.stream.IntStream;

@ExtendWith(TestWatcherExtension.class)
@Tag("run")
public class ProductDetailTest extends BaseTest
{
    @Test
    public void addBasketWithoutLogin() throws InterruptedException
    {
        HomePage homePage = new HomePage(driver,wait);
        homePage = homePage.waitForLayer(new HomePage(driver,wait));
        CategoriesPage categoriesPage = homePage.clickCategoriesBtn();
        ProductCategoriesPage productCategoriesPage = categoriesPage.clickFirstCategory();
        ProductListPage productListPage = productCategoriesPage.clickProduct();
        ProductPhotoPage productPhotoPage = productListPage.clickPhoto();
        Assertions.assertTrue(productPhotoPage.isDisplayedBack());

        IntStream.range(0, 2).forEach(i -> productPhotoPage.swipe());
        productListPage = productPhotoPage.clickBack();
        productListPage.addToBasket();
        Assertions.assertTrue(productListPage.isDisplayedAddToBasketFrame());

        BasketPage basketPage = productListPage.add()
                .clickBasket();
        Thread.sleep(8000L);
        Assertions.assertAll(
                () -> Assertions.assertTrue(basketPage.isDisplayedBasketItem(0)),
                () -> Assertions.assertEquals(1, basketPage.sizeOfBasketItems())
        );
    }
}
